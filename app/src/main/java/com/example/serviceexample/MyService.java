package com.example.serviceexample;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;




public class MyService extends Service {

    private static final String TAG = "serviceexample";
    private final IBinder myBinder = new myLocalBinder();

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    public String getCurrentTime () {
        SimpleDateFormat df =new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        Log.d(TAG,"getCurrentTime :" + df.format(new Date()));
        return (df.format(new Date()));


    }




    public class myLocalBinder extends Binder{

        MyService getService() {

            return MyService.this;

        }

    }


}
